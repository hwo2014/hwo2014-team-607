package noobbot.message;

import com.google.gson.JsonElement;
import noobbot.Entities.CarPosition;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CarPositionTest extends MsgTest {
    private final String jsonFile = "/carPosition.json";

    @Before
    public void setUp() throws Exception {
        super.setUp(jsonFile);
    }

    @Test
    public void testParse() throws Exception {
        JsonElement carPositionElement = gson.toJsonTree(json);

        CarPosition carPosition = gson.fromJson(carPositionElement, CarPosition.class);

        double delta = 0.0001;
        assertEquals("Angle", 40.0, carPosition.getAngle(), delta);
        assertEquals("Piece Index", 10, carPosition.getPieceIndex());
        assertEquals("Distance", 48.65, carPosition.getInPieceDistance(), delta);
    }
}