package noobbot.message;

import noobbot.message.client.AlternateJoin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class JoinTest extends MsgTest {
    private final String jsonFile = "/joinRace.json";

    @Before
    public void setUp() throws Exception {
        super.setUp(jsonFile);
    }

    @Test
    public void testParse() throws Exception {
        AlternateJoin join = gson.fromJson(json, AlternateJoin.class);
        assertEquals("msgType should be joinRace", "joinRace", join.msgType());
    }
}