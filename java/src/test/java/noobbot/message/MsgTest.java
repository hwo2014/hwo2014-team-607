package noobbot.message;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.Before;

import java.util.Scanner;

/**
 * Created by trigueros on 4/23/2014.
 */
public class MsgTest {
    protected Gson gson;
    protected JsonElement json;

    protected void setUp(String jsonFile) throws Exception {
        gson = new Gson();
        Scanner scanner = new Scanner(GameInitMsgTest.class.getResourceAsStream(jsonFile));

        StringBuilder builder = new StringBuilder();

        while( scanner.hasNext() )
            builder.append(scanner.nextLine());

        JsonParser parser = new JsonParser();
        json = parser.parse(builder.toString());
    }
}
