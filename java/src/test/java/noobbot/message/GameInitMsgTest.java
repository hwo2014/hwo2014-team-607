package noobbot.message;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import noobbot.Entities.Race;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Scanner;

/**
 * Created by trigueros on 4/20/2014.
 */
public class GameInitMsgTest extends MsgTest {
    private final String jsonFile = "/gameInit.json";

    @Before
    public void setUp() throws Exception {
        super.setUp(jsonFile);
    }

    @Test
    public void testParse() throws Exception {
        JsonElement raceElement = gson.toJsonTree(json.getAsJsonObject().get("data"));
        Race race = gson.fromJson(raceElement.getAsJsonObject().get("race"), Race.class);
        assertNotNull(race);

        assertEquals("Race Track Length", 3, race.getTrackLength());
        assertEquals("Switch Track", true, race.getTrackPiece(1).isSwitch());
    }
}
