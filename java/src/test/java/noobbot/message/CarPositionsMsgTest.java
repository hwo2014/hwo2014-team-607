package noobbot.message;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CarPositionsMsgTest extends MsgTest {
    private final String jsonFile = "/carPositions.json";

    @Before
    public void setUp() throws Exception {
        super.setUp(jsonFile);
    }

    @Test
    public void testParse() throws Exception {
        CarPositionsMsg carPositionsMsg = gson.fromJson(json, CarPositionsMsg.class);

        assertNotNull(carPositionsMsg);
        assertEquals("Size", 2, carPositionsMsg.getData().size());
        assertEquals("Tick", 1234, carPositionsMsg.getTick());
    }
}