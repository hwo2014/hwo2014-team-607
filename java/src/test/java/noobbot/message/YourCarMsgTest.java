package noobbot.message;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class YourCarMsgTest extends MsgTest {
    private final String jsonFile = "/yourCar.json";

    @Before
    public void setUp() throws Exception {
        super.setUp(jsonFile);
    }

    @Test
    public void testParse() throws Exception {
        YourCarMsg yourCarMsg = gson.fromJson(json, YourCarMsg.class);
        assertNotNull(yourCarMsg);
        assertEquals("Car Name", "Schumacher", yourCarMsg.getCarId().getName());
    }
}