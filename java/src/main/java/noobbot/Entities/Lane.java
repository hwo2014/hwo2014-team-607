package noobbot.Entities;

/**
* Created by trigueros on 4/19/2014.
*/
public class Lane {
    @Override
    public String toString() {
        return "Lane{" +
                "distanceFromCenter=" + distanceFromCenter +
                ", index=" + index +
                '}';
    }

    private int distanceFromCenter;
    private int index;

    public int getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(int distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
