package noobbot.Entities;

import java.util.ArrayList;

/**
* Created by trigueros on 4/19/2014.
*/
public class Track {
    private String id;
    private String name;
    private ArrayList<TrackPiece> pieces;
    private ArrayList<Lane> lanes;

    @Override
    public String toString() {
        return "Track{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", pieces=" + pieces +
                ", lanes=" + lanes +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<TrackPiece> getPieces() {
        return pieces;
    }

    public void setPieces(ArrayList<TrackPiece> pieces) {
        this.pieces = pieces;
    }

    public ArrayList<Lane> getLanes() {
        return lanes;
    }

    public void setLanes(ArrayList<Lane> lanes) {
        this.lanes = lanes;
    }
}
