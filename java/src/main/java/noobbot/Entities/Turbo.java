package noobbot.Entities;

/**
 * Created by Jose on 4/20/2014.
 */
public class Turbo {
    private double turboDurationMilliseconds;
    private double turboDurationTicks;
    private double turboFactor;

    public double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }

    public double getTurboDurationTicks() {
        return turboDurationTicks;
    }

    public double getTurboFactor() {
        return turboFactor;
    }
}
