package noobbot.Entities;

import java.util.ArrayList;

/**
* Created by trigueros on 4/19/2014.
*/
public class Race {
    private Track track;
    private ArrayList<Car> cars;
    /** TODO: add object for raceSession
    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }
     */

    /**
     * Since the track is circular, we can guarantee that the last track piece is connected
     * to the first track piece. Thus when getting a piece that is greater than {@code track.getPieces().size()}
     * , we can simply roll around to the beginning of the track list.
     */
    public TrackPiece getTrackPiece(int i) {
        return track.getPieces().get(Math.abs(i) % track.getPieces().size());
    }

    @Override
    public String toString() {
        return "Race{" +
                "track=" + track +
                ", cars=" + cars +
                '}';
    }

    public int getTrackLength() {
        return track.getPieces().size();
    }

    public Car getCar(String color) {
        Car matchingCar = new Car();
        for (int i = 0; i < cars.size(); i++) {
            Car car =  cars.get(i);
            if( car.getId().getColor().equals(color) ) {
                matchingCar = car;
                break;
            }
        }
        return matchingCar;
    }
}
