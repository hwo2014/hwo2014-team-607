package noobbot.Entities;

import noobbot.message.CarPositionsMsg;
import noobbot.message.client.LaneSwitch;
import noobbot.message.client.Ping;
import noobbot.message.client.SendMsg;

/**
* Created by trigueros on 4/19/2014.
*/
public class Car {
    private CarId id;
    private CarDimensions dimensions;

    private double angle;
    private long tick = -1;
    private int trackIndex;
    private double trackPosition;
    private double speed = 0.0;
    private int laneIndex = -1;

    /**
     * Given a {@code CarPositionsMsg}, extract the car's data and update
     * them
     */
    public void update(CarPositionsMsg carPositions) {
        if(carPositions == null)
            return;

        CarPosition position = carPositions.get(id);

        // Calculate current speed in terms of units/tick
        calculateSpeed(carPositions.getTick(), position.getInPieceDistance());

        trackIndex = position.getPieceIndex();
        tick = carPositions.getTick();
        trackPosition = position.getInPieceDistance();
        angle = position.getAngle();
        laneIndex = position.getEndLaneIndex();
    }

    @Override
    public String toString() {
        return "Car {" +
                "id=" + id +
                ", dimensions=" + dimensions +
                '}';
    }

    public CarId getId() {
        return id;
    }

    public void setId(CarId id) {
        this.id = id;
    }

    public CarDimensions getDimensions() {
        return dimensions;
    }

    public double getTrackPosition() {
        return trackPosition;
    }

    public long getTick() {
        return tick;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }

    public void setTrackPosition(double trackPosition) {
        this.trackPosition = trackPosition;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getSpeed() {
        return speed;
    }

    private void calculateSpeed(long tick, double pos) {
        long prevTick = this.tick;
        double prevPos = this.trackPosition;
        if( prevTick == -1 ) {
            this.tick = tick;
            this.trackPosition = pos;
        } else {
            this.speed = Math.abs(this.trackPosition - prevPos)/(this.tick - prevTick);
        }
    }

    public int getTrackIndex() {
        return trackIndex;
    }

    public SendMsg switchLane(String direction){

        if(direction.equalsIgnoreCase("left")){
            return new LaneSwitch("Left");
        }
        else if(direction.equalsIgnoreCase("right")){
            return new LaneSwitch("Right");
        }
        else {
            return new Ping();
        }
    }

    public class CarId {
        private String name;
        private String color;

        public String getName() {
            return name;
        }

        public String getColor() {
            return color;
        }

        @Override
        public String toString() {
            return "CarId{" +
                    "name='" + name + '\'' +
                    ", color='" + color + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CarId carId = (CarId) o;

            if (color != null ? !color.equals(carId.color) : carId.color != null) return false;
            if (name != null ? !name.equals(carId.name) : carId.name != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (color != null ? color.hashCode() : 0);
            return result;
        }
    }

    private class CarDimensions {
        public final double length;
        public final double width;
        public final double guideFlagPosition;

        private CarDimensions(double length, double width, double guideFlagPosition) {
            this.length = length;
            this.width = width;
            this.guideFlagPosition = guideFlagPosition;
        }

        @Override
        public String toString() {
            return "CarDimensions{" +
                    "length=" + length +
                    ", width=" + width +
                    ", guideFlagPosition=" + guideFlagPosition +
                    '}';
        }
    }
}
