package noobbot.Entities;

/**
 * Created by trigueros on 4/19/2014.
 */
public class CarPosition {
    private Car.CarId id;
    private double angle;
    private PiecePosition piecePosition;

    @Override
    public String toString() {
        return "CarPosition{" +
                "id=" + id +
                ", angle=" + angle +
                ", piecePosition=" + piecePosition +
                '}';
    }

    public int getPieceIndex() {
        return piecePosition.pieceIndex;
    }

    public double getInPieceDistance() {
        return piecePosition.inPieceDistance;
    }

    public int getLap() {
        return piecePosition.lap;
    }

    public Car.CarId getId() {
        return id;
    }

    public int getStartLaneIndex() {
        return piecePosition.lane.startLaneIndex;
    }
    public int getEndLaneIndex() {
        return piecePosition.lane.endLaneIndex;
    }

    public void setId(Car.CarId id) {
        this.id = id;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    private class PiecePosition {
        private int pieceIndex;
        private double inPieceDistance;
        private int lap;
        private LanePair lane;

        @Override
        public String toString() {
            return "PiecePosition{" +
                    "pieceIndex=" + pieceIndex +
                    ", inPieceDistance=" + inPieceDistance +
                    ", lap=" + lap +
                    '}';
        }

        private class LanePair {
            private int startLaneIndex;
            private int endLaneIndex;
        }
    }
}
