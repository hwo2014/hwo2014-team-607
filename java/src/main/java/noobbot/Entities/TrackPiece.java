package noobbot.Entities;

import com.google.gson.annotations.SerializedName;

/**
* Created by trigueros on 4/19/2014.
*/
public class TrackPiece {
    private int length;
    @SerializedName("switch")
    private boolean isSwitch;
    private int radius;
    private double angle;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isSwitch() {
        return isSwitch;
    }

    public void setSwitch(boolean isSwitch) {
        this.isSwitch = isSwitch;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    @Override
    public String toString() {
        return "TrackPiece{" +
                "length=" + length +
                ", isSwitch=" + isSwitch +
                ", radius=" + radius +
                ", angle=" + angle +
                '}';
    }
}
