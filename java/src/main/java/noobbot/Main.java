package noobbot;

import java.io.*;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import noobbot.Entities.Car;
import noobbot.Entities.Race;
import noobbot.Entities.TrackPiece;
import noobbot.message.CarPositionsMsg;
import noobbot.message.YourCarMsg;
import noobbot.message.client.AlternateJoin;
import noobbot.message.client.Join;
import noobbot.message.client.LaneSwitch;
import noobbot.message.client.MsgWrapper;
import noobbot.message.client.Ping;
import noobbot.message.client.SendMsg;
import noobbot.message.client.Throttle;
import noobbot.message.client.Turbo;

public class Main {
    public static void main(String... args) throws IOException {
        boolean log = true;
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        try {
            String trackName = args[4];
            int carCount = Integer.parseInt(args[5]);
            new Main(reader, writer, new AlternateJoin(botName, botKey, trackName, carCount), log);

        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            new Main(reader, writer, new Join(botName, botKey), log);
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private final boolean log;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        this(reader, writer, join, false);
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join, boolean log) throws IOException {
        this.writer = writer;
        this.log = log;
        send(join);
        beginRace(reader);
    }

    private double speed = 0.0;

    private void beginRace(BufferedReader reader) throws IOException {
        String line = null;

        Car car = new Car();
        Race race = new Race();

        boolean isTurboReady = false;
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

            switch(msgFromServer.msgType)
            {
                // 1.1 Server sends us acknowledgment of our message join.
                case "join":
                    log("Joined Race.");
                    break;

                // 2. Server sends back your car colour.
                case "yourCar":
                    YourCarMsg yourCarMsg = gson.fromJson(line, YourCarMsg.class);
                    car.setId(yourCarMsg.getCarId());
                    log("Our car's colour: " + car.getId().getColor());
                    break;

                // 3. Server sends init message that contains:
                //   - track pieces, number of lanes, and starting position
                //   - list of cars
                case "gameInit":
                    // This is where some pre processing needs to be done. Figuring out what needs to be done etc.
                    race = gson.fromJson(gson.toJsonTree(msgFromServer.data).getAsJsonObject().get("race"), Race.class);
                    car = race.getCar(car.getId().getColor());
                    break;

                // 4. Server sends start message, race begins.
                case "gameStart":
                    break;

                // 5. Server sends car positions
                case "carPositions":
                    // Update car's attributes
                    car.update(gson.fromJson(line, CarPositionsMsg.class));

                    // See the future!
                    TrackPiece nextPiece  = race.getTrackPiece(car.getTrackIndex() + 1);
                    TrackPiece currentPiece = race.getTrackPiece(car.getTrackIndex());
                    TrackPiece superNextPiece = race.getTrackPiece(car.getTrackIndex() + 2);



                    // Switch lanes
                    if(nextPiece.isSwitch()) {
                        if(currentPiece.getAngle() > 0){
                            send(car.switchLane("left"));
                        }

                        if(superNextPiece.getAngle() > 0){
                            send(car.switchLane("right"));
                        }

                        if(superNextPiece.getAngle() < 0){
                            send(car.switchLane("left"));
                        }
                    }

                    // La turbo
                    if( isTurboReady ) {
                        int minTracks = 5;
                        boolean useIt = false;
                        for(int i = 0; i < minTracks; i++) {
                            double trackAngle = race.getTrackPiece(car.getTrackIndex() + i).getAngle();
                            log("Track Angle: " + trackAngle);
                            useIt = trackAngle == 0.0;
                        }
                        if(useIt) {
//                            log("Using turbo!");
//                            log(turboMsg.toJson());
                            isTurboReady = false;
                            break;
                        }
                    }

                    // Adjust throttle
                    double throttle = (nextPiece.getAngle() != 0.0) ? 0.6 : 0.8;

//                    System.out.println("Sending a throttle of " + throttle);
                    // This is where we get current information about the car's position and decide what to do.

                    // 6. Send throttle value
                    send(new Throttle(throttle));
                    break;

                // 7. Server sends ending message that contains the final stats.
                case "gameEnd":
                    break;

                /** Additional messages */
                case "turboAvailable":
                    log("Turbo Available.");
//                    isTurboReady = true;
                    Turbo turboMsg = new Turbo("Gotta go fast!");
                    send(turboMsg);
                    break;

                case "tournamentEnd":
                    break;

                case "crash":
                    log("Crash Log");
                    log("Speed: " + car.getSpeed());
                    log("CarPosition: \n" + car.getTrackIndex() + " -> " + car.getTrackPosition());
                    break;

                case "spawn":
                    break;

                case "lapFinished":
                    log(line);
                    break;

                // Server sends dnf for disqualification etc.
                case "dnf":
                    log(line);
                    break;

                case "finish":
                    log(line);
                    break;

                /** Additional messages */

                default:
                    System.out.println(msgFromServer.msgType);
                    System.out.println(msgFromServer.data);
                    send(new Ping());
            }

            // Send a ping to ensure that a message got sent.
            ping();
        }
    }

    private void save(Object o, String filename) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filename)))) {
            bw.write(gson.toJson(o));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void ping() {
        send(new Ping());
    }

    private void log(String logMessage) {
        if(log)
            System.out.println(logMessage);
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

