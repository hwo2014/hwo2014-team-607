package noobbot.message.client;

/**
 * Created by trigueros on 4/23/2014.
 */
public class AlternateJoin extends SendMsg {
    private AlternateJoin.JoinData data;

    public AlternateJoin(String botName, String botKey, String trackName, int carCount) {
        data = new AlternateJoin.JoinData(new Join(botName, botKey), trackName, carCount);
    }

    @Override
    protected Object msgData() {
        return data;
    }

    @Override
    public String msgType() {
        return "joinRace";
    }

    private class JoinData {
        private Join botId;
        private String trackName;
        private int carCount;

        public JoinData(Join botId, String trackName, int carCount) {
            this.botId = botId;
            this.trackName = trackName;
            this.carCount = carCount;
        }
    }
}
