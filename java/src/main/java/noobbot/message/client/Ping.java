package noobbot.message.client;

/**
 * Created by trigueros on 4/23/2014.
 */
public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}
