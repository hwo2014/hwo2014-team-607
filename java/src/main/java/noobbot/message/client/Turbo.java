package noobbot.message.client;

/**
 * Created by trigueros on 4/23/2014.
 */
public class Turbo extends SendMsg {
    private final String message;

    public Turbo(String message) {
        this.message = message;
    }

    @Override
    protected Object msgData() {
        return message;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}
