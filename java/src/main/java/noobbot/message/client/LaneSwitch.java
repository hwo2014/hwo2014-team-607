package noobbot.message.client;

/**
 * Created by trigueros on 4/25/2014.
 */
public class LaneSwitch extends SendMsg {
    private String direction;

    public LaneSwitch(String direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
