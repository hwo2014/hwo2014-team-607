package noobbot.message;

import noobbot.Entities.Car;
import noobbot.Entities.CarPosition;

import java.util.ArrayList;

/**
 * Created by trigueros on 4/19/2014.
 */
public class CarPositionsMsg extends ServerMsg {
    private ArrayList<CarPosition> data;
    private long gameTick;

    @Override
    public String toString() {
        return "CarPositionsMsg{" +
                "data=" + data +
                ", gameTick=" + gameTick +
                '}';
    }

    public CarPosition get(Car.CarId car) {
        CarPosition result = new CarPosition();
        if( car != null )
            for(CarPosition pos: data) {
                if( pos.getId().equals(car) ) {
                    result = pos;
                    break;
                }
            }

        return result;
    }

    public ArrayList<CarPosition> getData() {
        return data;
    }

    public void setData(ArrayList<CarPosition> data) {
        this.data = data;
    }

    public long getTick() {
        return gameTick;
    }
}
