package noobbot.message;

/**
 * Created by trigueros on 4/19/2014.
 */
public class ServerMsg {
    protected String msgType;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }
}
