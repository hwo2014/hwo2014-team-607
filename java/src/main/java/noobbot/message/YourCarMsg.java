package noobbot.message;

import noobbot.Entities.Car;

/**
 * Created by trigueros on 4/19/2014.
 */
public class YourCarMsg extends ServerMsg {
    private Car.CarId data;

    @Override
    public String toString() {
        return "YourCarMsg{" +
                "data=" + data +
                '}';
    }

    public Car.CarId getCarId() {
        return data;
    }
}
